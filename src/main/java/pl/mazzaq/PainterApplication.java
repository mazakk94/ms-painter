package pl.mazzaq;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pl.mazzaq.filepainter.FilePainter;

import java.io.IOException;

public class PainterApplication {

    public static void main(String[] args) {
        if (args.length > 0) {
            String fileName = args[0];
            System.out.println("Provided filename: " + fileName);

            ApplicationContext ctx = new AnnotationConfigApplicationContext("pl.mazzaq"); // Use annotated beans from the specified package

            FilePainter filePainter = ctx.getBean(FilePainter.class);
            try {
                filePainter.paintFileWithName(fileName);
            } catch (IOException exception) {
                System.out.println("Error: file not found");
            }
        } else {
            System.out.println("Hello");
        }
    }
}

package pl.mazzaq.filepainter.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.mazzaq.filepainter.model.Character;
import pl.mazzaq.filepainter.model.ColoredCharacter;

import java.awt.*;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Component
public class CharactersPainter {

    @Autowired
    private final ColorResolver colorResolver;

    public CharactersPainter(ColorResolver colorResolver) {
        this.colorResolver = colorResolver;
    }

    public List<Character> paintText(String input) {
        CharacterIterator it = new StringCharacterIterator(input);
        List<Character> colors = new ArrayList<>();
        boolean isSecond = false;
        for (char simpleCharacter = it.first(); simpleCharacter != CharacterIterator.DONE; simpleCharacter = it.next()) {
            Optional<Color> color = colorResolver.resolveColor(simpleCharacter, isSecond);

            if (color.isPresent()) {
                colors.add(new ColoredCharacter(simpleCharacter, color.get()));
            } else {
                colors.add(new Character(simpleCharacter));
            }
            isSecond = flipIsSecondIfEven(isSecond, simpleCharacter);
        }
        return colors;
    }

    private boolean flipIsSecondIfEven(boolean isSecond, char character) {
        if (NumberHelper.isEvenDigit(character)) {
            isSecond = !isSecond;
        }
        return isSecond;
    }

}

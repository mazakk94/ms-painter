package pl.mazzaq.filepainter.helper;

import org.springframework.stereotype.Component;

import java.awt.*;
import java.util.Optional;

@Component
public class ColorResolver {

    public Optional<Color> resolveColor(char character, boolean isSecond) {
        if (Character.isLetter(character)) {
            return Optional.of(Color.GREEN);
        } else if (NumberHelper.isEvenDigit(character) && isSecond) {
            return Optional.of(Color.BLUE);
        } else if (Character.isDigit(character)) {
            return Optional.of(Color.RED);
        }
        return Optional.empty();
    }
}

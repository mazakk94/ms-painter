package pl.mazzaq.filepainter.helper;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import static java.nio.charset.StandardCharsets.UTF_8;

@Component
public class TextFileStringExtractor {

    public String getStringFromFile(String path) throws IOException {
        return getStringFromFile(path, UTF_8);
    }

    public String getStringFromFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
}

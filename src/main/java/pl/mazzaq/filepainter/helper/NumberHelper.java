package pl.mazzaq.filepainter.helper;

import lombok.experimental.UtilityClass;

@UtilityClass
public class NumberHelper {

    public static boolean isEvenDigit(char character) {
        boolean even = (character / 2) * 2 == character;
        return Character.isDigit(character) && even;
    }
}

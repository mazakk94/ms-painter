package pl.mazzaq.filepainter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.mazzaq.filepainter.helper.CharactersPainter;
import pl.mazzaq.filepainter.helper.TextFileStringExtractor;
import pl.mazzaq.filepainter.model.Character;

import java.io.IOException;
import java.util.List;

@Component
public class FilePainter {

    @Autowired
    private final TextFileStringExtractor extractor;

    @Autowired
    private final CharactersPainter charactersPainter;

    public FilePainter(TextFileStringExtractor extractor, CharactersPainter charactersPainter) {
        this.extractor = extractor;
        this.charactersPainter = charactersPainter;
    }

    public void paintFileWithName(String filename) throws IOException {
        String text = extractor.getStringFromFile(filename);

        List<Character> paintedCharacters = charactersPainter.paintText(text);

        paintedCharacters.forEach(System.out::println);
    }
}
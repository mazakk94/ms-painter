package pl.mazzaq.filepainter.model;

public class Character {

    protected final char character;

    public Character(char character) {
        this.character = character;
    }

    public char getCharacter() {
        return character;
    }

    @Override
    public String toString() {
        return String.valueOf(character);
    }
}


package pl.mazzaq.filepainter.model;

import java.awt.*;

public class ColoredCharacter extends Character {

    private final Color color;

    public ColoredCharacter(char character, Color color) {
        super(character);
        this.color = color;
    }

    @Override
    public String toString() {
        return character + " color: " + color.toString();
    }

    public Color getColor() {
        return color;
    }
}

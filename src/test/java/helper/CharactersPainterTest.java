package helper;

import org.junit.Test;
import pl.mazzaq.filepainter.helper.CharactersPainter;
import pl.mazzaq.filepainter.helper.ColorResolver;
import pl.mazzaq.filepainter.model.Character;
import pl.mazzaq.filepainter.model.ColoredCharacter;

import java.awt.*;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

public class CharactersPainterTest {

    private final ColorResolver colorResolver = new ColorResolver();
    private final CharactersPainter sut = new CharactersPainter(colorResolver);

    @Test
    public void shouldOnlyPaintOnGreen() {
        //when
        java.util.List<Character> coloredCharacters = sut.paintText("text....,,,,aaaa");

        //then
        java.util.List<Color> colors = extractColors(coloredCharacters);
        assertThat(colors).containsOnly(Color.GREEN);
    }

    @Test
    public void shouldPaintSecondEvenNumberOnBlue() {
        //when
        java.util.List<Character> coloredCharacters = sut.paintText("12.028");

        //then
        java.util.List<Color> colors = extractColors(coloredCharacters);
        assertThat(colors).containsSequence(Color.RED, Color.RED, Color.BLUE, Color.RED, Color.BLUE);
    }

    @Test
    public void shouldPaintOnlyOneLetter() {
        //when
        java.util.List<Character> coloredCharacters = sut.paintText(",,,..4785f,,,?////]]]{}}}[[*}");

        //then
        java.util.List<ColoredCharacter> greenCharacters = extractGreenLetters(coloredCharacters);
        assertThat(greenCharacters).hasSize(1);
        assertThat(greenCharacters.get(0).getColor()).isEqualTo(Color.GREEN);
        assertThat(greenCharacters.get(0).getCharacter()).isEqualTo('f');
    }

    private java.util.List<Color> extractColors(java.util.List<Character> coloredCharacters) {
        return coloredCharacters.stream()
                .filter(character -> character instanceof ColoredCharacter)
                .map(character -> ((ColoredCharacter) character).getColor())
                .collect(toList());
    }

    private java.util.List<ColoredCharacter> extractGreenLetters(java.util.List<Character> coloredCharacters) {
        return coloredCharacters.stream()
                .filter(character -> character instanceof ColoredCharacter)
                .map(character -> new ColoredCharacter(character.getCharacter(), ((ColoredCharacter) character).getColor()))
                .filter(character -> Color.GREEN.equals(character.getColor()))
                .collect(toList());
    }
}
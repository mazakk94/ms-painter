
**About:**

simple java app which paints characters from a `file` in following rules
```
if is letter: 
    GREEN
else if
    if is digit:
        if is every second even digit:
            BLUE
        else 
            RED

```

**Run locally**
1. `git clone https://gitlab.com/mazakk94/ms-painter.git`
2. `cd ms-painter`

3. create file, for example by 
`echo '1123abc000' > testfile.txt`
4. 'paint' letters
`java -jar painter.jar "testfile.txt"`


expected behavior:
```
Provided filename: testfile.txt
1 color: java.awt.Color[r=255,g=0,b=0]
1 color: java.awt.Color[r=255,g=0,b=0]
2 color: java.awt.Color[r=255,g=0,b=0]
3 color: java.awt.Color[r=255,g=0,b=0]
a color: java.awt.Color[r=0,g=255,b=0]
b color: java.awt.Color[r=0,g=255,b=0]
.
,
c color: java.awt.Color[r=0,g=255,b=0]
0 color: java.awt.Color[r=0,g=0,b=255]
0 color: java.awt.Color[r=255,g=0,b=0]
0 color: java.awt.Color[r=0,g=0,b=255]

```
